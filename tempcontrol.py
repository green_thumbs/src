#!/usr/bin/python

import time
import math
import board
import busio
import adafruit_am2320
import RPi.GPIO as GPIO
#from adafruit_seesaw.seesaw import Seesaw

testing = 1
GPIO.setup(11,GPIO.OUT)
GPIO.setup(13,GPIO.OUT)

# create the I2C shared bus of both soil and air sensors
i2c = busio.I2C(board.SCL, board.SDA)
am = adafruit_am2320.AM2320(i2c)
#ss = Seesaw(i2c, 0x36)

# Create temperature limits, import from a config file??
#max_temp = 25
#min_temp = 15

# check heat temperature
def sensor_check():
    return bool(am)
    
def get_temp():
    temp = int(am.temperature)
    return temp

def heat_on():
# Turn on heat pin
    GPIO.output(11, GPIO.HIGH)

def heat_off():
#Turn off heat pin
    GPIO.output(11, GPIO.LOW)
    
def cold_on():
# Turn on A/C pin
    GPIO.output(13, GPIO.HIGH)

def cold_off():
#Turn off A/C pin
    GPIO.output(13, GPIO.LOW)
   
# Test Code   
if(testing):
    print ("Testing functions, please wait...")
    i2c.scan()
    cold_off()
    heat_off()
    time.sleep(1)
    cold_on()
    heat_on()
    time.sleep(2)
    cold_off()
    heat_off()
    time.sleep(1)
    tmp = get_temp()
    print("Functions tested. Temperature is",tmp,", Feel free to use sensor.")
    
    while(True):
        if (get_temp() < 25):
            heat_on()
            time.sleep(5)
        elif (get_temp() > 30):
            cold_on()
            time.sleep(5)
        else:
            cold_off()
            heat_off()
            time.sleep(5)
            