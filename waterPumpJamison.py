
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

dry = 17  #IO pin setup for sensor and pump
med = 27
wet = 22
pump = 23

GPIO.setup(dry, GPIO.IN) #Dry sensor
GPIO.setup(med, GPIO.IN) #Moderate sensor
GPIO.setup(wet, GPIO.IN) #High moisture sensor
GPIO.setup(pump, GPIO.OUT) #Water pump control

outputMessage = """Welcome to the gardener
Please press 0, 1, 2, or 3
0: Exit
1: Dry setting
2: Moderate setting
3: Wet setting
Choose one of the above: """

cs = 0
ns = 0
rst = 1

while(1):
  if rst:
    cs = 0
    ns = 0
    rst = 0
    flag = 1

  if (cs == 0):
    flag = 1
    setting = int(input(outputMessage))
    if (setting == 0):
      exit()
    else:
      ns = setting
  if (cs == 1):
    while(flag):
      if (GPIO.input(dry)):
        GPIO.output(pump,GPIO.HIGH)
      else:
        GPIO.output(pump,GPIO.LOW)
        flag = 0
    ns = 0
  if (cs == 2):
    while(flag):
      if (GPIO.input(med)):
        GPIO.output(pump,GPIO.HIGH)
      else:
        GPIO.output(pump,GPIO.LOW)
        flag = 0
    ns = 0
  if (cs == 3):
    while(flag):
      if (GPIO.input(wet)):
        GPIO.output(pump,GPIO.HIGH)
      else:
        GPIO.output(pump,GPIO.LOW)
        flag = 0
    ns = 0
  if (cs > 3):
    print("\nWrong value entered\n\n")
    ns = 0
    
  cs = ns
