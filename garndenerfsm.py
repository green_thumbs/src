#!/usr/bin/python
import Rpi.GPIO as GPIO
import time
import tempcontrol
import water
# import other libraries?

#GPIO Pins
ac = 11 #activates blue LED to represent air conditioner
heat = 13 #activates red LED to represent heater
temp1 = 15 #temp. sensor to check if too cold
temp2 = 25 #temp. sensor to check if too hot
moisture = 17 #checks if watering is necessary with soil moisture sensor
pump = 23 #water pump
light = 27 #Controls plant's light source

#Setting as inputs/outputs
GPIO.setup(temp1,GPIO.IN)
GPIO.setup(temp2,GPIO.IN)
GPIO.setup(moisture,GPIO.IN)

GPIO.setup(ac,GPIO.OUT)
GPIO.setup(heat,GPIO.OUT)
GPIO.setup(pump,GPIO.OUT)
GPIO.setup(light,GPIO.OUT)

#initialize finite state machine with reset
reset = 1
flag = 1

checkCycle = 10 #in minutes how often to check for needs
checkCycle = checkCycle*60 #in seconds

timeOn = 12 #timeOn is amount of hours for light to be on every 24
timeOff =  24-timeOn

timeOn = timeOn*60*60 #in seconds
timeOff = timeOff*60*60

GPIO.output(light.GPIO.HIGH)#turn on light
startTime = time.time()#start timer to keep track of time light is on
L = True #light is on

# State Machine States
# S0 - Initialized. Add any init functions to this state
#   ALways Leads to S1
# S1 - Light check. Timer initialized in S0 determines on/off status
#   Always leads to S2
# S2 - Soil Check. Read soil sensor for any need of water
#   if dry, go to S3. Else, go to S4
# S3 - Watering plant. Power to mini pump
#   Always leads back to S4 after short delay
# S4 - Hot temperature check
#   if hot, to S5. else, to S1
# S5 - Fan and AC/Mister active
#   always leads to S1
# S6 - Cold Temperature check
#   If cold, to S7. Else, S1
# S7 - Fan and Heat active

while (flag):
    #Checks if light needs to toggle on/off based on time settings
    if(L == False):
        if(time.time()-startTime >= timeOff):
            GPIO.output(light.GPIO.HIGH)
            startTime = time.time()
            L = True
            reset = 1
    else:
        if(time.time()-startTime >= timeOn):
            GPIO.output(light.GPIO.LOW)
            startTime = time.time()
            L = False
            reset = 1
    if reset:
        #current_state = 'idleLightsOff'
        if(L==False):
            next_state = 'idleLightsOff'
        else:
            next_state = 'idleLightsOn'
        reset = 0
    else:
        current_state = next_state

    #I just followed the FSM diagram
    #I think becuase the lights are handled outside the state
    #machine we can combine some of the state to reduce code.
    #however, leaving it like this allows for a skeleton of
    #that makes later changes easier
    
    #I made some slight alterations to the FSM to make the coding easier
    #because the functions will probably run in a loop until the sensors deactivate
    
    if   cs == 'idleLightsOff':#everything off/lights are off
        if(GPIO.input(temp1) or GPIO.input(temp2)):#check if temps need adjusting
            ns = 'tempLightsOff'
        elif(GPIO.input(moisture)):#check if needs water
            ns = 'waterLightsOff'
        else:
            ns = cs
        time.sleep(checkCycle)
    elif cs == 'idleLightsOn':#lights are on only
        if(GPIO.input(temp1) or GPIO.input(temp2)):#check if temp needs adjusting
            ns = 'tempLightsOn'
        elif(GPIO.input(moisture)):
            ns = 'waterLightsOn'
        else:
            ns = cs
        time.sleep(checkCycle)
    elif cs == 'tempLightsOff':#fixing the temp
        aaronsTempFunction()
        #both still need fixing(water and temp)
        if((GPIO.input(temp1) or GPIO.input(temp2)) and (GPIO.input(moisture)))):
            ns = 'bothLightsOff'
        elif(GPIO.input(moisture)):#now needs water
            ns = 'waterLightsOff'
        elif(GPIO.input(temp1) or GPIO.input(temp2)):#temp still needs fixing
            ns = cs
        else:#everything is good
            ns = 'idleLightsOff'
    elif cs == 'waterLightsOff':#watering the plant
        jamisonsWaterFunction(userInput, dry, med, wet)
        if((GPIO.input(temp1) or GPIO.input(temp2)) and (GPIO.input(moisture)))):
            ns = 'bothLightsOff'
        elif(GPIO.input(moisture)):
            ns = cs
        elif(GPIO.input(temp1) or GPIO.input(temp2)):
            ns = 'tempLightsOff'
        else:
            ns = 'idleLightsOff'
    elif cs == 'bothLightsOff':#watering plant and fixing temp
        aaronsTempFunction()
        jamisonsWaterFunction(userInput, dry, med, wet)
        if((GPIO.input(temp1) or GPIO.input(temp2)) and (GPIO.input(moisture)))):
            ns = cs
        elif(GPIO.input(moisture)):
            ns = 'waterLightsOff'
        elif(GPIO.input(temp1) or GPIO.input(temp2)):
            ns = 'tempLightsOff'
        else:
            ns = 'idleLightsOff'
    elif cs == 'tempLightsOn':#lights on/fixing the temp
        aaronsTempFunction()
        if((GPIO.input(temp1) or GPIO.input(temp2)) and (GPIO.input(moisture)))):
            ns = 'bothLightsOn'
        elif(GPIO.input(moisture)):
            ns = 'waterLightsOn'
        elif(GPIO.input(temp1) or GPIO.input(temp2)):
            ns = cs
        else:
            ns = 'idleLightsOn'
    elif cs == 'waterLightsOn':#lights on/watering the plant
        jamisonsWaterFunction(userInput, dry, med, wet)
        if((GPIO.input(temp1) or GPIO.input(temp2)) and (GPIO.input(moisture)))):
            ns = 'bothLightsOn'
        elif(GPIO.input(moisture)):
            ns = cs
        elif(GPIO.input(temp1) or GPIO.input(temp2)):
            ns = 'tempLightsOn'
        else:
            ns = 'idleLightsOn'
    elif cs == 'bothLightsOn':#lights on/fixing the temp and watering the plant
        aaronsTempFunction()
        jamisonsWaterFunction(userInput, dry, med, wet)
        if((GPIO.input(temp1) or GPIO.input(temp2)) and (GPIO.input(moisture)))):
            ns = cs
        elif(GPIO.input(moisture)):
            ns = 'waterLightsOn'
        elif(GPIO.input(temp1) or GPIO.input(temp2)):
            ns = 'tempLightsOn'
        else:
            ns = 'idleLightsOn'
    else:
        reset = 1
